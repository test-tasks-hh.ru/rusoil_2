<?php

use Bitrix\Main\Loader;

const EXTRANET_NO_REDIRECT = true;
const STOP_STATISTICS = true;
const BX_SECURITY_SHOW_MESSAGE = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
const NO_KEEP_STATISTIC = 'Y';
const NO_AGENT_STATISTIC = 'Y';
const NO_AGENT_CHECK = true;
const DisableEventsCheck = true;

global $USER, $APPLICATION;

try {
    Loader::includeModule('highloadblock');
} catch (\Bitrix\Main\LoaderException $e) {
    die($e->getMessage());
}

if(!function_exists('__W4aActionsEndResponse'))
{
    function __W4aActionsEndResponse($result)
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);
        if(!empty($result))
        {
            echo CUtil::PhpToJSObject($result);
        }
        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
        die();
    }
}

/**
 * ONLY 'POST' SUPPORTED
 */

if (!$USER->IsAuthorized() || !check_bitrix_sessid() || $_SERVER['REQUEST_METHOD'] != 'POST')
{
    __W4aActionsEndResponse(array('ERROR' => 'ACCESS_DENIED'));
}

CUtil::JSPostUnescape();
$APPLICATION->RestartBuffer();
header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);
$mode = $_POST['MODE'] ?? '';
if(!isset($mode[0]))
{
    __W4aActionsEndResponse(array('ERROR'=>'MODE_NOT_FOUND_0'));
}

CBitrixComponent::includeComponentClass("w4a:vacancy");

$arResult = array();
switch($mode)
{
    case 'RESPOND':
        $elId = (int)$_REQUEST['elId'];
        if(empty($elId))
        {
            __W4aActionsEndResponse(array('ERROR'=>'ID_IS_EMPTY'));
        }
        if(
            empty($_REQUEST['formData']['fio'])
            || empty($_REQUEST['formData']['phone'])
            || empty($_REQUEST['formData']['comment'])
            || empty($_REQUEST['formData']['file'])

        )
        {
            __W4aActionsEndResponse(array('ERROR'=>'NOT_FULL_DATA'));
        }

        $userId = $USER->GetID();
        $ar = CW4aVacancy::getVacancyById($elId);
        $arUsers = json_decode($ar[$elId]['UF_USERS']);
        if(!in_array($userId, $arUsers)){
            $arUsers[] =  $userId;
        }
        $arFields = [
            'UF_USERS' => json_encode($arUsers)
        ];
        $result = CW4aVacancy::update($elId, $arFields);
        $arResult = [
            'DATA' => [
                'elId' => $elId,
                'result' => $result,
                'test' => [
                    'userId' => $USER->GetID(),
                    '$ar' => $ar,
                    '$arUsers' => $arUsers,
                ],
            ]
        ];
        break;

    default:
        __W4aActionsEndResponse(array('ERROR'=>'MODE_NOT_FOUND:' . $mode));
        break;
}

$arResult = array(
    'RESULT' => $arResult,
    'POST' => $_POST,
    'ERROR' => false,
);

__W4aActionsEndResponse($arResult);