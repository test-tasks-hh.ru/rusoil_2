<?php

use Bitrix\Main\LoaderException;
use CBitrixComponent;
use Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

class CW4aVacancy extends CBitrixComponent
{
    const HL_CODE = 'W4aVacancy';

    /**
     * @throws LoaderException
     * @throws TasksException
     * @throws CTaskAssertException
     */
    public function executeComponent(): void
    {
        global $USER;
        Loader::includeModule('highloadblock');
        $hlId = self::getHlId();

        $this->arResult = array(
            'ITEMS' => self::getVacancies($hlId),
            'ACCESS' => self::getAccess(),
            'USER_ID' => $USER->GetID(),
            'TEST' => [
                'HL_ID' => $hlId,

            ],

        );
        $this->includeComponentTemplate();
    }

    /**
     * @return bool
     */
    private static function getAccess(): bool
    {
        global $USER;
        if (!$USER->IsAuthorized())
        {
            return false;
        }
        return true;
    }
    /**
     * @param $hlId
     * @return array
     */
    private static function getVacancies($hlId): array
    {
        $arFilter = array();
        $arOrder = array("ID" => "DESC");
        return self::getHlById($hlId, $arFilter, $arOrder);
    }
    /**
     * @param $hlId
     * @param array $arFilter
     * @param array $arOrder
     * @param bool $uniqField
     * @param int $limit
     * @return array|false
     */
    private static function getHlById($hlId, $arFilter = array(), $arOrder = array(), $uniqField = false, $limit = 0)
    {
        Loader::includeModule("highloadblock");
        if(empty($hlId)) // ID HLBLOCK
            return false;

        if(empty(count($arOrder)))
            $arOrder = array("ID" => "ASC");

        $arParams = array(
            "select" => array("*"),
            "order" => $arOrder,
            "filter" => $arFilter,
        );
        if(!empty(intval($limit)))
            $arParams['limit'] = $limit;

        $hlBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList($arParams);
        $arData = array();
        while($ar = $rsData->Fetch())
        {

            if($limit == 1)
                $arData= $ar;
            elseif(!empty($uniqField))
                $arData[$ar[$uniqField]] = $ar;
            else
                $arData[] = $ar;
        }

        return $arData;
    }

    /**
     * @return bool|int
     */
    private static function getHlId(){
        return self::GetHlIDByCode(self::HL_CODE);
    }

    /**
     * @param string $code
     * @return bool|int
     * @throws \Bitrix\Main\LoaderException
     */
    private static function GetHlIDByCode(string $code)
    {
        global $USER;
        $cache = \Bitrix\Main\Data\Cache::createInstance(); // получаем экземпляр класса
        $ttl = 3600 * 24;
        $uniqueString = serialize($code . "|" . serialize($USER->GetUserGroupArray()));
        $initDir = "GetHlIdByCode";        //false
        $baseDir = "cache";
        if ( $cache->initCache($ttl, $uniqueString, $initDir, $baseDir) ) { // проверяем кеш и задаём настройки
            $vars = $cache->getVars();                                        // достаем переменные из кеша
            return intval($vars[ "ID" ]);
        } elseif ( $cache->startDataCache() ) {
            if ( !Loader::includeModule('highloadblock') ) {
                $cache->abortDataCache();
                return false;
            }
            if ( 0 == strlen(trim($code)) ) {
                $cache->abortDataCache();
                return false;
            }
            $result = HL\HighloadBlockTable::getList([ 'filter' => [ '=NAME' => $code ] ]);
            if ( $row = $result->fetch() ) {
                $cache->endDataCache([ "ID" => intval($row[ "ID" ]) ]); // записываем в кеш
                return intval($row[ "ID" ]);
            } else {
                $cache->abortDataCache();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $hlId
     * @param $id
     * @param $arFields
     * @return bool|array
     */
    public static function update($id, $arFields)
    {
        Loader::includeModule("highloadblock");
        $hlId = self::getHlId();

        $hlBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
        $entity_data_class = $entity->getDataClass();

        $res = $entity_data_class::update($id, $arFields);
        if($res->isSuccess()) {
            return true;
        }
        else
        {
            return $res->getErrors();
        }
    }

    public static function getVacancyById($id): array
    {
        Loader::includeModule("highloadblock");
        $hlId = self::getHlId();

        $arFilter = array('ID'=>$id);
        $arOrder = array("ID" => "DESC");
        return self::getHlById($hlId, $arFilter, $arOrder, $uniqField = 'ID');
    }
}
