<?php

$MESS["VACANCY_RESPONDED"] = "Вы уже откликнулись!!!";
$MESS['ERROR_DEFAULT'] = "Ошибка!!!";
$MESS['VACANCY_NO_AUTH'] = "Необходимо авторизоваться";
$MESS['TEXT_VACANCY_FORM'] = "Форма отправки резюме";
$MESS['VACANCY_FORM_SUBMIT'] = "Отправить";
$MESS['VACANCY_FORM_CANCEL'] = "Отмена";
$MESS['VACANCY_FORM_FIO'] = "Фамилия Имя Отчество";
// номер телефона, резюме (файл), отклик (текст).
$MESS['VACANCY_FORM_PHONE'] = "Номер телефона";
$MESS['VACANCY_FORM_RESUME'] = "Резюме (файл)";
$MESS['VACANCY_FORM_COMMENT'] = "Отклик";
$MESS['NOT_FULL_DATA'] = '<span class="ui-alert-message"><strong>Внимание!</strong> Не все данные заполнены!!!</span>';