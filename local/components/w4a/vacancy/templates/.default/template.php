<?php
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $templateFolder */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
Extension::load("ui.buttons");
Extension::load("ui.forms");
Extension::load("ui.notification");
Extension::load("ui.dialogs.messagebox");
CJSCore::RegisterExt(
    'W4aVacancy',
    array(
        "lang" => $templateFolder."/script.js.php",
        'rel' => [ 'jquery' ]
    )
);
CJSCore::Init(array("W4aVacancy"));
CJSCore::Init(['popup', 'date']);

$access = $arParams['ACCESS'];

$sessionId = bitrix_sessid();
$id = 0;

$editorCfg = array(
    'id' => $id,
    'sessId' => $sessionId,
    'serviceUrl' => $componentPath . '/ajax.php?' . bitrix_sessid_get(),
    'siteId' => SITE_ID,
    'ownerType' => 'VACANCY',
    'viewMode' => 'VACANCY_RESPOND',
    'viewType' => 'VACANCY_RESPOND',
    'access' => $arResult['ACCESS'],
);

$config = \CUtil::PhpToJSObject($editorCfg);
$js = "<script type='text/javascript'>" . "BX.W4aVacancyOn = {}; BX.ready(function(){BX.W4aVacancyOn = BX.W4aVacancy.create($id, $config)});" . "</script>";
Asset::getInstance()->addString($js);
?>

<h1><?=Loc::getMessage('VACANCY');?></h1>
<div class="w4a-vacancy">
    <?
    $nColor = 1;
    foreach ($arResult['ITEMS'] as $item):
        $icon = mb_substr(ltrim($item['UF_TITLE']),0,1);
        $arUsers = json_decode($item['UF_USERS']);
        $isResponded = false;
        if(!empty($arResult['USER_ID']) && in_array($arResult['USER_ID'],$arUsers))
            $isResponded = true;
    ?>
    <div class="item color-<?=$nColor?>">
        <div>
            <h2><?=$item['UF_TITLE']?></h2>
        </div>
        <div>
            <div class="icon"><?=$icon?></div>
            <div class="content">
                <div class="description"><?=$item['UF_DESCRIPTION']?></div>
                <div class="buttons">
                    <?if($isResponded):?>
                        <button class="ui-btn ui-btn-primary-dark ui-btn-disabled" data-id="<?=$item['ID']?>"><?=Loc::getMessage('VACANCY_RESPONDED');?></button>

                    <?else:?>
                        <button class="ui-btn ui-btn-primary-dark w4a-respond" data-id="<?=$item['ID']?>"><?=Loc::getMessage('VACANCY_RESPOND');?></button>

                    <?endif;?>
                </div>
            </div>
        </div>
    </div>
    <?
        $nColor = ($nColor==1)?2:1;
    endforeach;?>
</div>