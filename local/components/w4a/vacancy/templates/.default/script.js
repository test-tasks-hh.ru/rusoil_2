BX.namespace("BX");
if (typeof(BX.W4aVacancy) === "undefined") {
    BX.W4aVacancy = function () {
        this._id = 0;
        this._settings = {};
        this._serviceUrl = "";
        this._access = false;

        this.formMsgId = 'w4a-form-msg';
        this.isShowFormPopup = false;
    };
    BX.W4aVacancy.prototype = {
        initialize: function (id, config) {
            this._id = id;
            this._settings = config ? config : {};
            this._serviceUrl = this.getSetting('serviceUrl', '');
            if (!BX.type.isNotEmptyString(this._serviceUrl)) {
                throw 'BX.W4aVacancy: could not find service URL.';
            }
            this._access = this.getSetting('access', false);
            this.setEvents();
            //console.log(this);
        },
        getSetting: function (name, defaultval) {
            return typeof (this._settings[name]) != 'undefined' ? this._settings[name] : defaultval;
        },
        setSetting: function (name, value) {
            this._settings[name] = value;
        },
        setEvents: function(){
            let obj = $('button.w4a-respond');
            if(typeof obj === 'undefined' || obj === null || obj.length === 0)
                return false;
            for( let i=0; i < obj.length; i++){
                let btn = obj[i];
                if(btn)
                {
                    BX.bind(
                        btn,
                        "click",
                        BX.delegate(this.setVacancyForm, this)
                    );
                }
            }
        },
        getFormData: function(){
            let fio = $(BX("w4a-form-fio")).val();
            let phone = $(BX("w4a-form-phone")).val();
            let comment = $(BX("w4a-form-comment")).val();
            let file = $(BX("w4a-form-file")).val();
            return {
                'fio': fio,
                'phone': phone,
                'comment': comment,
                'file': file,
            };
        },
        handleRespond: function(event){
            if(!this._access){
                alert(BX.message('VACANCY_NO_AUTH'));
                return false;
            }
            let btn = event.target
            let id = BX.data(btn, 'id');
            if(typeof id === 'undefined' || id === null || id === ''){
                return false;
            }
            let formData = this.getFormData();
            let postData = {
                'MODE': 'RESPOND',
                'OWNER_TYPE': this.getSetting('ownerType', ''),
                'PERMISSION_ENTITY_TYPE': this.getSetting('permissionEntityType', ''),
                'sessId': this.getSetting('sessId', ''),
                'elId': id,
                'formData': formData,
            };
            BX.ajax({
                'url': this._serviceUrl,
                'method': "POST",
                'dataType': "json",
                'data': postData,
                onsuccess: BX.delegate(this._onActionRequestSuccess, this),
                onfailure: BX.delegate(this._onActionRequestFailure, this)
            });

        },
        setResponded: function(id){
            let btn = $('button[data-id="'+id+'"]');
            let msg = BX.message('VACANCY_RESPONDED');
            $(btn).html(msg);
            $(btn).addClass('ui-btn-disabled')
        },

        setVacancyForm: function(event){
            if(!this._access){
                alert(BX.message('VACANCY_NO_AUTH'));
                return false;
            }
            let self = this;
            let btn = event.target
            let id = BX.data(btn, 'id');
            if(typeof id === 'undefined' || id === null || id === ''){
                return false;
            }
            let popup = BX.PopupWindowManager.create("w4a-popup-form", BX('element'), {
                content: '<div id="' + self.formMsgId + '" class="ui-alert ui-alert-danger" style="display:none;"  >' + BX.message('NOT_FULL_DATA') + '</div>',
                width: 400, // ширина окна
                height: 520, // высота окна
                zIndex: 100, // z-index
                closeIcon: {
                    // объект со стилями для иконки закрытия, при null - иконки не будет
                    opacity: 1
                },
                titleBar: BX.message('TEXT_VACANCY_FORM'),
                closeByEsc: true, // закрытие окна по esc
                darkMode: false, // окно будет светлым или темным
                autoHide: false, // закрытие при клике вне окна
                draggable: true, // можно двигать или нет
                resizable: false, // можно ресайзить
                min_height: 300, // минимальная высота окна
                min_width: 300, // минимальная ширина окна
                lightShadow: true, // использовать светлую тень у окна
                angle: false, // появится уголок
                overlay: {
                    // объект со стилями фона
                    backgroundColor: 'black',
                    opacity: 500
                },
                buttons: [
                    new BX.PopupWindowButton({
                        text: BX.message('VACANCY_FORM_SUBMIT'), // текст кнопки
                        id: 'w4a-send-btn', // идентификатор
                        className: 'ui-btn ui-btn-success', // доп. классы
                        events: {
                            click: function() {
                                // Событие при клике на кнопку
                                self.handleRespond(event);
                                //self.handleFormSend();
                            }
                        }
                    }),
                    new BX.PopupWindowButton({
                        text:  BX.message('VACANCY_FORM_CANCEL'),
                        id: 'w4a-close-btn',
                        className: 'ui-btn ui-btn-light-border',
                        events: {
                            click: function() {
                                popup.close();
                            }
                        }
                    })
                ],
                events: {
                    onPopupShow: function() {
                        // Событие при показе окна
                        if(!self.isShowFormPopup){
                            self.isShowFormPopup = true;
                        }
                            let obj = BX(self.formMsgId);
                            BX.insertBefore(
                                BX.create('form', {
                                attrs: {
                                    id: "w4a-vacancy-form-data",
                                    name:"w4a-vacancy-form",
                                    method:"POST",
                                    ENCTYPE:"multipart/form-data",
                                    action: "send.php",
                                    className: 'w4a-wb-form-container',
                                },
                                children: [
                                    BX.create({
                                        tag: 'input',
                                        props: {
                                            name: "id",
                                            type:'hidden',
                                            value: id,
                                        },
                                    }),
                                    BX.create({
                                        tag: 'div',
                                        props: {
                                            className: "w4a-item"
                                        },

                                        children: [
                                            BX.create({
                                                tag:'div',
                                                props: {
                                                    className: "ui-ctl ui-ctl-textbox"
                                                },
                                                children: [
                                                    BX.create({
                                                        tag: 'input',
                                                        props: {
                                                            id: "w4a-form-fio",
                                                            name: "fio",
                                                            type:'text',
                                                            className:'ui-ctl-element',
                                                            value: "",
                                                            placeholder:BX.message('VACANCY_FORM_FIO')
                                                        },
                                                    }),
                                                ]
                                            }),
                                        ]
                                    }),
                                    BX.create({
                                        tag: 'div',
                                        props: {
                                            className: "w4a-item"
                                        },
                                        children: [
                                            BX.create({
                                                tag:'div',
                                                props: {
                                                    className: "ui-ctl ui-ctl-textbox"
                                                },
                                                children: [
                                                    BX.create({
                                                        tag: 'input',
                                                        props: {
                                                            id: "w4a-form-phone",
                                                            name: "phone",
                                                            type:'text',
                                                            className:'ui-ctl-element',
                                                            value: "",
                                                            placeholder:BX.message('VACANCY_FORM_PHONE')
                                                        },
                                                    }),
                                                ]
                                            }),
                                        ]
                                    }),
                                    BX.create({
                                        tag: 'div',
                                        props: {
                                            className: "w4a-item"
                                        },
                                        children: [
                                            BX.create({
                                                tag:'div',
                                                props: {
                                                    className: "ui-ctl  ui-ctl-textarea ui-ctl-resize-y"
                                                },
                                                children: [
                                                    BX.create({
                                                        tag: 'textarea',
                                                        props: {
                                                            id: "w4a-form-comment",
                                                            name: "comment",
                                                            className:'ui-ctl-element',
                                                            value: "",
                                                            placeholder:BX.message('VACANCY_FORM_COMMENT')
                                                        },
                                                    }),
                                                ]
                                            }),
                                        ]
                                    }),
                                    /**
                                     <label class="ui-ctl ui-ctl-file-btn">
                                     <input type="file" class="ui-ctl-element">
                                     <div class="ui-ctl-label-text">Добавить фотографию</div>
                                     </label>
                                     */
                                    BX.create({
                                        tag: 'div',
                                        props: {
                                            className: "w4a-item"
                                        },
                                        children: [
                                            BX.create({
                                                tag:'label',
                                                props: {
                                                    className: "ui-ctl ui-ctl-file-btn"
                                                },
                                                children: [
                                                    BX.create({
                                                        tag: 'input',
                                                        props: {
                                                            id: "w4a-form-file",
                                                            name: "file",
                                                            type:'file',
                                                            className:'ui-ctl-element',
                                                            value: "",
                                                        },
                                                    }),
                                                    BX.create({
                                                        tag: 'div',
                                                        props: {
                                                            className:'ui-ctl-label-text',
                                                        },
                                                        text:BX.message('VACANCY_FORM_RESUME')
                                                    }),
                                                ]
                                            }),
                                        ]
                                    }),
                                ],
                            }), obj);
                    },
                    onPopupClose: function() {
                        // Событие при закрытии окна
                        popup.destroy();
                    }
                }
            });
            popup.show();
            this.popupRequestForm = popup;
        },
        handleFormSend: function(){
            let form = BX("w4a-vacancy-form-data");
            form.submit();
        },
        _onActionRequestSuccess:function(data){
            $(BX(this.formMsgId)).hide();
            if (data.ERROR) {
                if(data.ERROR === "NOT_FULL_DATA")
                    $(BX(this.formMsgId)).show();

                this.error(data);
                return;
            }
            switch (data['POST']['MODE'])
            {
                case 'RESPOND':
                    //console.log(data);
                    let id = parseInt(data['RESULT']['DATA']['elId']);
                    this.setResponded(id);
                    this.handleFormSend();
                    break;
                default:
                    data.ERROR = "JS_MODE_NOT_FOUND" + ' ' + data['POST']['MODE'];
                    this.error(data);
                    break;
            }
        },
        _onActionRequestFailure:function(data) {
            console.log('dataFailure');
            console.log(data);
        },
        error: function (data){
            console.log('Error');
            console.log(data);
        },
    };
    BX.W4aVacancy.create = function (id, config){
        let self = new BX.W4aVacancy();
        self.initialize(id, config);
        return self;
    };
}