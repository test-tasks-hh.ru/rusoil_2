<?php

use Bitrix\Main\LoaderException;
use CBitrixComponent;
use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

class CW4aRusoilFormRequest extends CBitrixComponent
{
    /**
     * @throws LoaderException
     * @throws TasksException
     * @throws CTaskAssertException
     */
    public function executeComponent(): void
    {
        $this->arResult = array(
            'WH' => self::getWareHouses(),
            'BRANDS' => self::getBrands(),
            'TABLE_STRUCTURE' => self::getTableStructure(),
            'TEST' => [1,2,3],

        );
        $this->includeComponentTemplate();
    }

    /**
     * В этом методе получаем список складов из Б24
     * @return array
     */
    private static function getWareHouses(): array
    {
        $whCount = 5;
        $arResult[] = [
            'ID' => 0,
            'NAME' => '(выберите склад поставки)'
        ];
        for ($i = 1; $i <= $whCount; $i++){
            $arResult[] = [
                'ID' => $i,
                'NAME' => 'Склад-' . $i
            ];
        }
        return $arResult;
    }

    /**
     * В этом методе получаем список брендов из Б24
     * @return array
     */
    private static function getBrands(): array
    {
        $bCount = 9;
        $arResult[] = [
            'ID' => 0,
            'NAME' => 'Выберите бренд'
        ];
        for ($i = 1; $i <= $bCount; $i++){
            $arResult[] = [
                'ID' => $i,
                'NAME' => 'Бренд-' . $i
            ];
        }
        return $arResult;
    }

    /**
     * Возвращает структуру колонок таблицы
     * TYPE - тип данных
     * W - ширина колонки
     * @return array[]
     */
    private static function getTableStructure(): array
    {
        return [
            ['TYPE' => 'BRAND', 'W' => 2],
            ['TYPE' => 'PRODUCT', 'W' => 3],
            ['TYPE' => 'QTY', 'W' => 2],
            ['TYPE' => 'PACK', 'W' => 2],
            ['TYPE' => 'CLIENT', 'W' => 2],
            ['TYPE' => 'BUTTON', 'W' => 1],
        ];
    }
}
