<?php
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $templateFolder */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
Extension::load("ui.buttons");
Extension::load("ui.forms");
Extension::load("ui.notification");
Extension::load("ui.dialogs.messagebox");
CJSCore::RegisterExt(
    'W4aRusoilFormRequest',
    array(
        "lang" => $templateFolder."/script.js.php",
        'rel' => [ 'jquery' ]
    )
);
CJSCore::Init(array("W4aRusoilFormRequest"));
CJSCore::Init(['popup', 'date']);
Bitrix\Main\Page\Asset::getInstance()->addCss( $componentPath . "/ext/bootstrap/css/bootstrap.min.css");
Bitrix\Main\Page\Asset::getInstance()->addJs( $componentPath . "/ext/bootstrap/js/bootstrap.bundle.min.js");
$access = $arParams['ACCESS'];

$sessionId = bitrix_sessid();
$id = 0;

$editorCfg = array(
    'id' => $id,
    'sessId' => $sessionId,
    'serviceUrl' => $componentPath . '/ajax.php?' . bitrix_sessid_get(),
    'siteId' => SITE_ID,
    'ownerType' => 'RUSOIL',
    'viewMode' => 'RUSOIL_FORM',
    'viewType' => 'RUSOIL_FORM_REQUEST',
    'wh' => $arResult['WH'],
    'brands' => $arResult['BRANDS'],
    'tableStructure' => $arResult['TABLE_STRUCTURE'],
);

$config = \CUtil::PhpToJSObject($editorCfg);
$js = "<script type='text/javascript'>" . "BX.W4aRusoilFormRequestOn = {}; BX.ready(function(){BX.W4aRusoilFormRequestOn = BX.W4aRusoilFormRequest.create($id, $config)});" . "</script>";
Asset::getInstance()->addString($js);
?>

<main>
    <div class="container">
        <form id="w4a-request-form-data" class="needs-validation w4a-rusoil" novalidate method="POST" ENCTYPE="multipart/form-data" action="./">
            <div class="form-group">
                <div class="w4a-group-title"><?=Loc::getMessage('FORM_TITLE');?></div>
                <div class="col-md-4">
                    <label for="w4a-title" class="form-label"><?=Loc::getMessage('FORM_REQUEST_TITLE');?></label>
                    <input id="w4a-title" type="text" name="title" class="form-control" placeholder="<?=Loc::getMessage('FORM_REQUEST_TITLE');?>" required>

                    <div class="invalid-feedback w4a-padding-l-15">
                        <?=Loc::getMessage('FORM_REQUIRED_FIELD', array("#FIELD#" => Loc::getMessage('FORM_REQUEST_TITLE')));?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="w4a-group-title"><?=Loc::getMessage('FORM_REQUEST_CATEGORY');?></div>
                <div class="my-3">
                    <div class="form-check">
                        <input id="category-oil" name="category" value="<?=Loc::getMessage('FORM_REQUEST_CATEGORY_OIL');?>" type="radio" class="form-check-input" required>
                        <label class="form-check-label" for="category-oil"><?=Loc::getMessage('FORM_REQUEST_CATEGORY_OIL');?></label>
                    </div>
                    <div class="form-check">
                        <input id="category-while" name="category" value="<?=Loc::getMessage('FORM_REQUEST_CATEGORY_WHILE');?>" type="radio" class="form-check-input" required>
                        <label class="form-check-label" for="category-while"><?=Loc::getMessage('FORM_REQUEST_CATEGORY_WHILE');?></label>

                        <div class="invalid-feedback">
                            <?=Loc::getMessage('FORM_REQUIRED_FIELD', array("#FIELD#" => Loc::getMessage('FORM_REQUEST_CATEGORY')));?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="w4a-group-title"><?=Loc::getMessage('FORM_REQUEST_TYPE');?></div>
                <div class="my-3">
                    <div class="form-check">
                        <input id="type-price" name="type" value="<?=Loc::getMessage('FORM_REQUEST_TYPE_PRICE');?>" type="radio" class="form-check-input" required>
                        <label class="form-check-label" for="type-price"><?=Loc::getMessage('FORM_REQUEST_TYPE_PRICE');?></label>
                    </div>
                    <div class="form-check">
                        <input id="type-wh" name="type" value="<?=Loc::getMessage('FORM_REQUEST_TYPE_WH');?>" type="radio" class="form-check-input" required>
                        <label class="form-check-label" for="type-wh"><?=Loc::getMessage('FORM_REQUEST_TYPE_WH');?></label>
                    </div>
                    <div class="form-check">
                        <input id="type-spec" name="type" value="<?=Loc::getMessage('FORM_REQUEST_TYPE_SPEC');?>" type="radio" class="form-check-input" required>
                        <label class="form-check-label" for="type-spec"><?=Loc::getMessage('FORM_REQUEST_TYPE_SPEC');?></label>
                        <div class="invalid-feedback">
                            <?=Loc::getMessage('FORM_REQUIRED_FIELD', array("#FIELD#" => Loc::getMessage('FORM_REQUEST_TYPE')));?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="w4a-group-title">
                    <label for="w4a-wh" class="form-label"><?=Loc::getMessage('FORM_REQUEST_WH');?></label>
                </div>
                <div class="col-md-4">
                    <select id="w4a-wh" name="wh" class="form-select">
                        <?foreach ($arResult['WH'] as $val):?>
                        <option value="<?=($val['ID'] == 0)?$val['ID']:$val['NAME'];?>"><?=$val['NAME'];?></option>
                        <?endforeach;?>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="w4a-group-title"><?=Loc::getMessage('FORM_REQUEST_CONTENT');?></div>
                <div class="col-12">
                    <div class="row mb-1 text-center w4a-table-title">
                        <div class="col-2 themed-grid-col"><?=Loc::getMessage('FORM_TABLE_BRAND');?></div>
                        <div class="col-3 themed-grid-col"><?=Loc::getMessage('FORM_TABLE_PRODUCT');?></div>
                        <div class="col-2 themed-grid-col"><?=Loc::getMessage('FORM_TABLE_QTY');?></div>
                        <div class="col-2 themed-grid-col"><?=Loc::getMessage('FORM_TABLE_PACK');?></div>
                        <div class="col-2 themed-grid-col"><?=Loc::getMessage('FORM_TABLE_CLIENT');?></div>
                        <div class="col-1 themed-grid-col">[...]</div>
                    </div>

                    <div id="row-0" class="row mb-2 text-center">
                        <div class="col-2 themed-grid-col">
                            <select name="ITEMS[0][BRAND]" class="form-select">
                                <?foreach ($arResult['BRANDS'] as $val):?>
                                    <option value="<?=($val['ID'] == 0)?$val['ID']:$val['NAME'];?>"><?=$val['NAME'];?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                        <div class="col-3 themed-grid-col">
                            <input name="ITEMS[0][PRODUCT]" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-2 themed-grid-col">
                            <input name="ITEMS[0][QTY]" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-2 themed-grid-col">
                            <input name="ITEMS[0][PACK]" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-2 themed-grid-col">
                            <input name="ITEMS[0][CLIENT]" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-1 themed-grid-col w4a-buttons">
                            <button class="btn btn-success btn-sm w4a-add-row" data-row="0" type="button">+</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-4" />

            <div class="form-group">
                <input type="file" name="file" class="form-control-file">
            </div>

            <div class="form-group">
                <label for="w4a-comment"><?=Loc::getMessage('FORM_REQUEST_COMMENT');?></label>
                <textarea id="w4a-comment" name="comment" class="form-control" rows="5"></textarea>
            </div>


            <div class="form-group">
                <button name="submit" class="w-100 btn btn-primary btn-lg" type="submit"><?=Loc::getMessage('FORM_REQUEST_SEND');?></button>
            </div>
        </form>

    </div>
</main>
<script src="<?=$componentPath?>/ext/bootstrap/js/form-validation.js"></script>

<?php
if(isset($_REQUEST['submit'])):
    $fileId = 0;
    /** Регистрируем файл  */
    if(!empty($_FILES['file']['size']))
        $fileId = \CFile::SaveFile($_FILES['file'],"mailatt");


    $mes = "<b>".Loc::getMessage('FORM_REQUEST_TITLE').":</b> " . $_REQUEST['title'] .'<br />';
    $mes .= "<b>".Loc::getMessage('FORM_REQUEST_CATEGORY').":</b> " . $_REQUEST['category'] .'<br />';
    $mes .= "<b>".Loc::getMessage('FORM_REQUEST_TYPE').":</b> " . $_REQUEST['type'] .'<br />';
    $mes .= "<b>".Loc::getMessage('FORM_REQUEST_WH').":</b> " . (empty($_REQUEST['wh'])?Loc::getMessage('FORM_MSG_NOT_SET'):$_REQUEST['wh']) .'<br />';
    $mes .= "<b>".Loc::getMessage('FORM_REQUEST_CONTENT').":</b> <br>";

    $itemsHtml = '<table>';
    $itemsHtml .= '<tr>';
    foreach ($arResult['TABLE_STRUCTURE'] as $type){
        if($type['TYPE'] == 'BUTTON')
            continue;

        $itemsHtml .= '<td style="border-width:1px">'. Loc::getMessage('FORM_TABLE_' . $type['TYPE']) .'</td>';
    }
    $itemsHtml .= '</tr>';
    foreach ($_REQUEST['ITEMS'] as $item){
        $itemsHtml .= '<tr>';
        foreach ($arResult['TABLE_STRUCTURE'] as $type){
            if($type['TYPE'] == 'BUTTON')
                continue;
            $itemsHtml .= '<td style="border-width:1px">'. $item[$type['TYPE']] .'</td>';
        }
        $itemsHtml .= '</tr>';
    }
    $itemsHtml .= '</table>';

    $mes .= $itemsHtml;
    $mes .= "<b>".Loc::getMessage('FORM_REQUEST_COMMENT').":</b> " . $_REQUEST['comment'] .'<br />';
    /** Шлем письмо с вложенным файлом */
    \Bitrix\Main\Mail\Event::send(array(
        "EVENT_NAME" => "VM_SERVICE_REQUEST",
        "LID" => "s1",
        "C_FIELDS" => array(
            "EMAIL" => "a@w4.kz",
            "MESSAGE" => $mes,
        ),
        "FILE" => array($fileId),
    ));


    $mes = str_replace("'", '', $mes);
?>
<script type="application/javascript">
    const messageBox = BX.UI.Dialogs.MessageBox.create(
        {
            message: '<?=$mes?>',
            title: '<?=Loc::getMessage('FORM_REQUEST_IS_OK')?>',
            buttons: BX.UI.Dialogs.MessageBoxButtons.OK,
            onOk: function(messageBox)
            {
                messageBox.close();
            },
        }
    );

    messageBox.show();
</script>
<?php
/** Удаляем файл  */
    if($fileId)
        CFile::Delete($fileId);
endif;