<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['FORM_TITLE'] = 'Новая заявка';
$MESS['FORM_REQUEST_SEND'] = 'Отправить';
$MESS['FORM_REQUIRED_FIELD'] = 'Требуется заполнить поле: <strong>#FIELD#</strong>';
$MESS['FORM_REQUEST_IS_OK'] = "Ваша Заявка отправлена";

$MESS['FORM_REQUEST_TITLE'] = 'Заголовок заявки';
$MESS['FORM_REQUEST_CATEGORY'] = 'Категория';
$MESS['FORM_REQUEST_TYPE'] = 'Вид заявки';
$MESS['FORM_REQUEST_WH'] = 'Склад поставки';
$MESS['FORM_REQUEST_CONTENT'] = 'Состав заявки';
$MESS['FORM_REQUEST_COMMENT'] = 'Комментарий';

$MESS['FORM_REQUEST_CATEGORY_OIL'] = 'Масла, Автохимия, фильтры, обогреватели, запчасти, сопутствующие товары';
$MESS['FORM_REQUEST_CATEGORY_WHILE'] = 'Шины, диски';

$MESS['FORM_REQUEST_TYPE_PRICE'] = 'Запрос цены и сроков поставки';
$MESS['FORM_REQUEST_TYPE_WH'] = 'Пополнение склада';
$MESS['FORM_REQUEST_TYPE_SPEC'] = 'Спецзаказ';

$MESS['FORM_TABLE_BRAND'] = 'Бренд';
$MESS['FORM_TABLE_PRODUCT'] = 'Наименование';
$MESS['FORM_TABLE_QTY'] = 'Кол-во';
$MESS['FORM_TABLE_PACK'] = 'Фасовка';
$MESS['FORM_TABLE_CLIENT'] = 'Клиент';
$MESS['FORM_MSG_NOT_SET'] = 'Не указан';