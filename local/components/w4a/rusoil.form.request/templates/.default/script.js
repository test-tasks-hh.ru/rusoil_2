BX.namespace("BX");
if (typeof(BX.W4aRusoilFormRequest) === "undefined") {
    BX.W4aRusoilFormRequest = function () {
        this._id = 0;
        this._settings = {};
        this._serviceUrl = "";
        this._tableStructure = [];
    };
    BX.W4aRusoilFormRequest.prototype = {
        initialize: function (id, config) {
            this._id = id;
            this._settings = config ? config : {};
            this._serviceUrl = this.getSetting('serviceUrl', '');
            if (!BX.type.isNotEmptyString(this._serviceUrl)) {
                throw 'BX.W4aRusoilFormRequest: could not find service URL.';
            }
            this._tableStructure = this.getSetting('tableStructure', []);

            this.setEvents();
            // console.log(this);
        },
        getSetting: function (name, defaultval) {
            return typeof (this._settings[name]) != 'undefined' ? this._settings[name] : defaultval;
        },
        setSetting: function (name, value) {
            this._settings[name] = value;
        },
        setEvents: function(){
            let obj = $('button.w4a-add-row');
            if(typeof obj === 'undefined' || obj === null || obj.length === 0)
                return false;
            for( let i=0; i < obj.length; i++){
                let btn = obj[i];
                if(btn)
                {
                    BX.bind(
                        btn,
                        "click",
                        BX.delegate(this.addRow, this)
                    );
                }
            }
        },
        addRow: function(event){
            let self = this;
            let objBtn = {};
            if( typeof event.target !== 'undefined') {
                objBtn = event.target
            }else if(typeof event.target === 'undefined' && typeof event === 'object') {
                objBtn = event;
            }else{
                return false;
            }

            let nRowCurrent = parseInt( BX.data(objBtn, 'row'));
            let nRowNext = (nRowCurrent + 1);
            let obj = BX('row-' + nRowCurrent);
            BX.insertBefore(
                self.getRow(nRowNext),
                obj
            );
            this.replaceEvent(objBtn);
        },
        delRow: function(objBtn){
            let nRow = parseInt(BX.data(objBtn, 'row'));
            BX.UI.Dialogs.MessageBox.show(
                {
                    message: BX.message("FORM_DEL_QUESTION") + nRow + "?",
                    modal: true,
                    buttons: BX.UI.Dialogs.MessageBoxButtons.OK_CANCEL,
                    onOk: function(messageBox)
                    {
                        BX.remove(BX('row-' + nRow));
                        BX.UI.Notification.Center.notify({
                            content: BX.message('FORM_DEL_OK') + nRow
                        });
                        messageBox.close();
                    },
                    onCancel: function(messageBox)
                    {
                        messageBox.close();
                    },
                }
            );
        },
        replaceEvent: function(objBtn){
            let self = this;
            let btn = BX.create({
                tag: 'button',
                props: {
                    className: 'btn btn-danger btn-sm w4a-del-row',
                    type:'button',
                },
                dataset: {
                    row: parseInt( BX.data(objBtn, 'row'))
                },
                html: '-',
                events: {
                    click: function() {
                        // Событие при клике на кнопку
                        self.delRow(this);
                    }
                }
            });
            BX.insertBefore(
                btn,
                objBtn
            );
            BX.remove(objBtn);
        },
        getRow: function(nRow){
            let self = this;
            let cols = [];
            for(let i=0; i<this._tableStructure.length; i++){
                cols.push( self.getCel( nRow, this._tableStructure[i]['TYPE'], this._tableStructure[i]['W']) );
            }
            return BX.create({
                tag: 'div',
                attrs: {
                    id: "row-" + nRow,
                    className:'row mb-2 text-center',
                },
                children: cols
            });
        },
        getCel: function(nRow, t, w){
            let self = this;
            let html = '';
            switch (t){
                case 'BUTTON':
                    /**
                     <button class="btn btn-success btn-sm w4a-add-row" data-row="0" type="button">+</button>
                     */
                    html = BX.create({
                        tag: 'button',
                        props: {
                            className: 'btn btn-success btn-sm w4a-add-row',
                            type:'button',
                        },
                        dataset: {
                            row: nRow
                        },
                        html: '+',
                        events: {
                            click: function() {
                                // Событие при клике на кнопку
                                self.addRow(this);
                            }
                        }
                    });
                    break;

                case 'BRAND':
                    /**
                     <select class="form-select">
                     <option value="0">Выберите бренд</option>
                     <option value="Бренд-1">Бренд-1</option>
                     <option value="Бренд-2">Бренд-2</option>
                     </select>
                     */
                    let brands = this.getSetting('brands', []);
                    let options = [];
                    for ( let i = 0; i < brands.length; i++ ){
                        options.push(
                            BX.create({
                                tag: 'option',
                                props: {
                                    value: (i === 0)? 0 : brands[i]['NAME'],
                                    className: 'form-select',
                                },
                                html: brands[i]['NAME'],
                            })
                        );
                    }
                    html = BX.create({
                        tag: 'select',
                        props: {
                            name: "ITEMS["+nRow+"]" + "[" + t +"]",
                            className: 'form-select',
                        },
                        children: options,
                    });
                    break;
                default:
                    html = BX.create({
                            tag: 'input',
                            props: {
                                name: "ITEMS["+nRow+"]" + "[" + t +"]",
                                type:'text',
                                value: "",
                                className: 'form-control',
                            },
                        });
                    break;
            }
            return BX.create({
                tag: 'div',
                attrs: {
                    className:'col-' + w + ' themed-grid-col',
                },
                children: [html],

            });
        },
    };
    BX.W4aRusoilFormRequest.create = function (id, config){
        let self = new BX.W4aRusoilFormRequest();
        self.initialize(id, config);
        return self;
    };
}