<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ваше резюме отправлено");
?>
   <h1>Ваше резюме отправлено</h1>

<?
$fileId = 0;
if(!empty($_FILES['file']['size']))
    $fileId = \CFile::SaveFile($_FILES['file'],"mailatt");

CBitrixComponent::includeComponentClass("w4a:vacancy");

$id = (int)$_REQUEST['id'];
$ar = CW4aVacancy::getVacancyById($id);

$vacTitle = $ar[$id]['UF_TITLE'];


$mes = "<b>Вакансия:</b> " . $vacTitle .'<br />';
$mes .= '<b>ФИО: </b>' . $_REQUEST['fio'] .'<br />';
$mes .= '<b>Телефон:</b> ' . $_REQUEST['phone'] .'<br />';
$mes .= '<b>Отклик:</b> ' . $_REQUEST['comment'] .'<br />';

\Bitrix\Main\Mail\Event::send(array(
    "EVENT_NAME" => "VM_SERVICE_REQUEST",
    "LID" => "s1",
    "C_FIELDS" => array(
        "EMAIL" => "a@w4.kz",
        "MESSAGE" => "MESSAGE",
    ),
    "FILE" => array($fileId),
));

if($fileId)
    CFile::Delete($fileId);

?>
<div style="padding: 15px; font-size: 14px;">
    <?=$mes?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>